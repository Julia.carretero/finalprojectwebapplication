<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326092213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, personne_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE produit_carnet_producteurs DROP FOREIGN KEY FK_BE287EF81A0A5E6');
        $this->addSql('ALTER TABLE produit_carnet_producteurs DROP FOREIGN KEY FK_BE287EFF347EFB');
        $this->addSql('ALTER TABLE produit_carnet_producteurs ADD CONSTRAINT FK_BE287EF81A0A5E6 FOREIGN KEY (carnet_producteurs_id) REFERENCES carnetproducteurs (id)');
        $this->addSql('ALTER TABLE produit_carnet_producteurs ADD CONSTRAINT FK_BE287EFF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE produit_carnet_producteurs DROP FOREIGN KEY FK_BE287EFF347EFB');
        $this->addSql('ALTER TABLE produit_carnet_producteurs DROP FOREIGN KEY FK_BE287EF81A0A5E6');
        $this->addSql('ALTER TABLE produit_carnet_producteurs ADD CONSTRAINT FK_BE287EFF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_carnet_producteurs ADD CONSTRAINT FK_BE287EF81A0A5E6 FOREIGN KEY (carnet_producteurs_id) REFERENCES carnetproducteurs (id) ON DELETE CASCADE');
    }
}
